#!/bin/bash
#imxrt program filepath 0xXXXXXXXX
#imxrt erase_chip

ROOT=C:/Users/MeichiMC_Wang/Desktop
CHIP_NAME=$1
OP_CMD=$2
FILE_NAME=$3
FILE_PATH=${ROOT}/${FILE_NAME}
ADDRESS=$4

function showUsage()
{
	printf "tool.sh [chip name] [op cmd] [filepath] [address]\n"
	printf "[chip name]: support imxrt, lpc824, lpc8xx\n"
	printf "[op cmd]: program/erase_chip\n"
	printf "[filepath]: the path of the binary file\n"
	printf "[address]: 0xXXXXXXXX start program from 0x6000000\n"

	return 0
}

if [ ${CHIP_NAME} = "imxrt" ]; then
    if [ ${OP_CMD} = "program" ]; then
        #program 
        ./src/openocd.exe -f interface/cmsis-dap.cfg -c "adapter speed 3000" -f target/imxrt.cfg -s tcl -c "flash init; init; reset; halt" -c "program ${FILE_PATH} exit ${ADDRESS}" -c "init"
    elif [ "${OP_CMD}" = "erase_chip" ]; then
        #erase the chip
        ./src/openocd.exe -f imxrt_erase.cfg -s tcl
    else
        showUsage;
    fi
elif [ ${CHIP_NAME} = "lpc824" ] || [ ${CHIP_NAME} = "lpc8xx" ]; then
    if [ ${OP_CMD} = "program" ]; then
        ./src/openocd.exe -f interface/cmsis-dap.cfg -f target/lpc8xx.cfg -s tcl -c "program ${FILE_PATH} exit ${ADDRESS}"
    elif [ "${OP_CMD}" = "erase_chip" ]; then
        ./src/openocd.exe -f interface/cmsis-dap.cfg -f target/lpc8xx.cfg -s tcl -c "flash init; init; reset; halt" -c "flash erase_sector lpc8xx.flash 0 last" -c "init" -c "exit"
    fi
else
    showUsage;
fi

