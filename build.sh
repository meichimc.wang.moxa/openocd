#!/bin/bash

# Start by updating the package database and core system packages
pacman -Syu
# If MSYS2 closes, start it again
pacman -Su
# Install required dependencies
pacman -S mingw-w64-x86_64-toolchain git make libtool pkg-config autoconf automake texinfo mingw-w64-x86_64-libusb mingw-w64-x86_64-hidapi
# download openocd package
git clone git@gitlab.com:meichimc.wang.moxa/openocd.git
cd openocd
# start to build openocd.exe
./bootstrap
./configure --disable-werror --enable-cmsis-dap
make -j4
